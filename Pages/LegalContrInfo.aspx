﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="LegalContrInfo.aspx.cs" Inherits="ContractorWebForms.Pages.LegalContrInfo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <br /> <br />

    <table class="nav-justified" style="height: 546px">
        <tr>
            <td colspan="2">
                <asp:Label ID="lblTitle" runat="server" Text="Legal Contractor Information"></asp:Label>
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="width: 150px">
                <asp:Label ID="lblBIIN" runat="server" Text="BIIN"></asp:Label>
            </td>
            <td style="width: 236px">
                <asp:TextBox ID="txtBIIN" runat="server" Width="174px"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
            <td rowspan="5">
                <asp:GridView ID="dgViewLegalCtr" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="Id" DataSourceID="SqlDataSourceContractor" OnSelectedIndexChanged="GridView1_SelectedIndexChanged">
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" />
                        <asp:BoundField DataField="Id" HeaderText="Id" InsertVisible="False" ReadOnly="True" SortExpression="Id" />
                        <asp:BoundField DataField="BIIN" HeaderText="BIIN" SortExpression="BIIN" />
                        <asp:BoundField DataField="Created_date" HeaderText="Created_date" SortExpression="Created_date" />
                        <asp:BoundField DataField="Edited_date" HeaderText="Edited_date" SortExpression="Edited_date"/>
                        <asp:BoundField DataField="Created_by" HeaderText="Created_by" SortExpression="Created_by" />
                        <asp:BoundField DataField="Company_name" HeaderText="Company_name" SortExpression="Company_name" />
                    </Columns>
                </asp:GridView>
                <asp:SqlDataSource ID="SqlDataSourceContractor" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT * FROM [Legal_contractor]"></asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td style="width: 150px">
                <asp:Label ID="lblCrtDate" runat="server" Text="Created Date"></asp:Label>
            </td>
            <td style="width: 236px">
                <asp:TextBox ID="txtCrtDate" runat="server" Width="175px" TextMode="Date"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="width: 150px">
                <asp:Label ID="edtDate" runat="server" Text="Edited Date"></asp:Label>
            </td>
            <td style="width: 236px">
                <asp:TextBox ID="txtEdtDate" runat="server" Width="175px" TextMode="Date"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="width: 150px">
                <asp:Label ID="lblAuthor" runat="server" Text="Created By"></asp:Label>
            </td>
            <td style="width: 236px">
                <asp:TextBox ID="txtAuthor" runat="server" Width="175px"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="width: 150px">
                <asp:Label ID="lblComName" runat="server" Text="Company Name"></asp:Label>
            </td>
            <td style="width: 236px">
                <asp:TextBox ID="txtComName" runat="server" Width="175px"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="width: 150px">&nbsp;</td>
            <td style="width: 236px">
                <asp:Button ID="btnAdd" runat="server" Text="Add" OnClick="btnAdd_Click" />
                <asp:Button ID="btnUpdate" runat="server" Text="Update" OnClick="btnUpdate_Click" />
                <asp:Button ID="btnDelete" runat="server" Text="Delete" Width="75px" OnClick="btnDelete_Click" />
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="width: 150px">&nbsp;</td>
            <td style="width: 236px">
                <asp:Button ID="btnCancel" runat="server" Text="Cancel Operation" Width="182px" OnClick="btnCancel_Click" />
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="width: 150px">
                <asp:Label ID="lblSID" runat="server" Visible="False"></asp:Label>
            </td>
            <td style="width: 236px">
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="width: 150px">&nbsp;</td>
            <td style="width: 236px">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="width: 150px">&nbsp;</td>
            <td style="width: 236px">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="width: 150px">&nbsp;</td>
            <td style="width: 236px">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>

</asp:Content>
