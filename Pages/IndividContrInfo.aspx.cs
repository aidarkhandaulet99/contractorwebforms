﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace ContractorWebForms.Pages
{
    public partial class IndividContrInfo : System.Web.UI.Page
        {
        string cs = ConfigurationManager.ConnectionStrings["dbCon"].ConnectionString;
        SqlConnection con;
        SqlCommand cmd;
        SqlDataAdapter adapter;
        DataTable dt;

        public void DataLoad() 
        {
            if (Page.IsPostBack)
            {
                dgViewIndividCtr.DataBind();
            }
        }
        public void ClearAllData() 
        {
            txtIIN.Text = "";
            txtCrtDate.Text = DateTime.Today.Date.ToString();
            txtEdtDate.Text = DateTime.Today.Date.ToString();
            txtAuthor.Text = "";
            txtName.Text = "";
            txtSurname.Text = "";
            ddlGender.SelectedValue = ddlGender.Items[0].ToString();
        }


        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblSID.Text = dgViewIndividCtr.SelectedRow.Cells[1].Text;
            txtIIN.Text = dgViewIndividCtr.SelectedRow.Cells[2].Text;
            txtCrtDate.Text = dgViewIndividCtr.SelectedRow.Cells[3].Text;
            txtEdtDate.Text = dgViewIndividCtr.SelectedRow.Cells[4].Text;
            txtAuthor.Text = dgViewIndividCtr.SelectedRow.Cells[5].Text;
            txtName.Text = dgViewIndividCtr.SelectedRow.Cells[6].Text;
            txtSurname.Text = dgViewIndividCtr.SelectedRow.Cells[7].Text;
            ddlGender.Text = dgViewIndividCtr.SelectedRow.Cells[8].Text;

        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            if(txtIIN.Text!="" && txtName.Text!= "" && txtSurname.Text != "" && txtAuthor.Text != "")
           {
                using (con = new SqlConnection(cs))
                {
                    con.Open();
                    cmd = new SqlCommand("INSERT INTO Individ_contractor(IIN,Created_date,Edited_date,Created_by,Name,Surname,Gender)" +
                        " VALUES(@IIN,@CreatedDate, @EditedDate, @CreatedBy,@Name,@Surname,@Gender)", con);
                    cmd.Parameters.AddWithValue("@IIN", txtIIN.Text);
                    cmd.Parameters.AddWithValue("@CreatedDate", txtCrtDate.Text);
                    cmd.Parameters.AddWithValue("@EditedDate", txtEdtDate.Text);
                    cmd.Parameters.AddWithValue("@CreatedBy", txtAuthor.Text);
                    cmd.Parameters.AddWithValue("@Name", txtName.Text);
                    cmd.Parameters.AddWithValue("@Surname", txtSurname.Text);
                    cmd.Parameters.AddWithValue("@Gender", ddlGender.SelectedValue);
                   // cmd.Parameters.AddWithValue("@Id", lblSID.Text);
                    cmd.ExecuteNonQuery();
                    con.Close();
                    DataLoad();
                    ClearAllData();
                }
            }
            else
            {
                lblMessage.Text = "Fill All Information";
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {

            if (txtIIN.Text != "" && txtName.Text != "" && txtSurname.Text != "" && txtAuthor.Text != "")
            {
                using (con = new SqlConnection(cs))
                {
                    con.Open();
                    cmd = new SqlCommand("UPDATE Individ_contractor SET IIN=@IIN, Created_date=@CreatedDate, Edited_date = @EditedDate," +
                        "Created_by=@CreatedBy,Name=@Name,Surname=@Surname, Gender=@Gender WHERE Id = @Id", con);
                    cmd.Parameters.AddWithValue("@IIN", txtIIN.Text);
                    cmd.Parameters.AddWithValue("@CreatedDate", txtCrtDate.Text);
                    cmd.Parameters.AddWithValue("@EditedDate", txtEdtDate.Text);
                    cmd.Parameters.AddWithValue("@CreatedBy", txtAuthor.Text);
                    cmd.Parameters.AddWithValue("@Name", txtName.Text);
                    cmd.Parameters.AddWithValue("@Surname", txtSurname.Text);
                    cmd.Parameters.AddWithValue("@Gender", ddlGender.SelectedValue);
                    cmd.Parameters.AddWithValue("@Id", lblSID.Text);

                    cmd.ExecuteNonQuery();
                    con.Close();
                    DataLoad();
                    ClearAllData();
                }
            }
            else
            {
                lblMessage.Text = "Fill All Information";
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            using(con = new SqlConnection(cs))
            {
                con.Open();
                cmd = new SqlCommand("DELETE FROM Individ_contractor WHERE Id=@Id", con);
                cmd.Parameters.AddWithValue("@Id", lblSID.Text);
                cmd.ExecuteNonQuery();
                con.Close();
                DataLoad();
                ClearAllData();

            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ClearAllData();
        }
    }
}