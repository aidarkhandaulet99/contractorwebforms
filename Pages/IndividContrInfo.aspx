﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="IndividContrInfo.aspx.cs" Inherits="ContractorWebForms.Pages.IndividContrInfo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <br /> <br />

    <table class="nav-justified" style="height: 532px">
        <tr>
            <td colspan="2">
                <asp:Label ID="lblTitle" runat="server" Text="Individ Contractor Information"></asp:Label>
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblIIN" runat="server" Text="IIN"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtIIN" runat="server" Width="176px"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="width: 233px">
                <asp:Label ID="lblCrDate" runat="server" Text="Created Date"></asp:Label>
            </td>
            <td style="width: 274px">
                <asp:TextBox ID="txtCrtDate" runat="server" Width="176px" TextMode="Date"></asp:TextBox>
            </td>
            <td rowspan="8">
                <asp:GridView ID="dgViewIndividCtr" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="Id" DataSourceID="SqlDataSourceContractor" Height="208px" style="margin-left: 0px" Width="554px" OnSelectedIndexChanged="GridView1_SelectedIndexChanged">
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" />
                        <asp:BoundField DataField="Id" HeaderText="Id" InsertVisible="False" ReadOnly="True" SortExpression="Id" />
                        <asp:BoundField DataField="IIN" HeaderText="IIN" SortExpression="IIN" />
                        <asp:BoundField DataField="Created_date" HeaderText="Created_date" SortExpression="Created_date" />
                        <asp:BoundField DataField="Edited_date" HeaderText="Edited_date" SortExpression="Edited_date"  />
                        <asp:BoundField DataField="Created_by" HeaderText="Created_by" SortExpression="Created_by" />
                        <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
                        <asp:BoundField DataField="Surname" HeaderText="Surname" SortExpression="Surname" />
                        <asp:BoundField DataField="Gender" HeaderText="Gender" SortExpression="Gender" />
                    </Columns>
                </asp:GridView>
                <asp:SqlDataSource ID="SqlDataSourceContractor" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT * FROM [Individ_contractor]"></asp:SqlDataSource>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
            <td style="width: 233px">
                <asp:Label ID="lblEdtDate" runat="server" Text="Edited Date"></asp:Label>
            </td>
            <td style="width: 274px">
                <asp:TextBox ID="txtEdtDate" runat="server" Width="174px" TextMode="Date"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="width: 233px">
                <asp:Label ID="lblAuthor" runat="server" Text="Created By"></asp:Label>
            </td>
            <td style="width: 274px">
                <asp:TextBox ID="txtAuthor" runat="server" Width="175px"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="width: 233px">
                <asp:Label ID="lblName" runat="server" Text="Name"></asp:Label>
            </td>
            <td style="width: 274px">
                <asp:TextBox ID="txtName" runat="server" Height="22px" Width="175px"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="width: 233px">
                <asp:Label ID="lblSurname" runat="server" Text="Surname"></asp:Label>
            </td>
            <td style="width: 274px">
                <asp:TextBox ID="txtSurname" runat="server" Width="171px"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="width: 233px">
                <asp:Label ID="lblGender" runat="server" Text="Gender"></asp:Label>
            </td>
            <td style="width: 274px">
                <asp:DropDownList ID="ddlGender" runat="server">
                    <asp:ListItem>Choose any</asp:ListItem>
                    <asp:ListItem>Male</asp:ListItem>
                    <asp:ListItem>Female</asp:ListItem>
                </asp:DropDownList>
                <br />
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="width: 233px">&nbsp;</td>
            <td style="width: 274px">
                <asp:Button ID="btnAdd" runat="server" Text="Add" Width="47px" OnClick="btnAdd_Click" />
                <asp:Button ID="btnUpdate" runat="server" Text="Update" Width="68px" OnClick="btnUpdate_Click" />
                <asp:Button ID="btnDelete" runat="server" Text="Delete" Width="69px" OnClick="btnDelete_Click" />
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="width: 233px">&nbsp;</td>
            <td style="width: 274px">
                <asp:Button ID="btnCancel" runat="server" Text="Cancel Operation" Width="183px" OnClick="btnCancel_Click" />
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="height: 35px;">
                <asp:Label ID="lblSID" runat="server" Visible="False"></asp:Label>
            </td>
            <td style="height: 35px;">
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
            </td>
            <td style="height: 35px"></td>
            <td style="height: 35px"></td>
        </tr>
        <tr>
            <td style="width: 233px">&nbsp;</td>
            <td style="width: 274px">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="width: 233px">&nbsp;</td>
            <td style="width: 274px">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>

</asp:Content>
