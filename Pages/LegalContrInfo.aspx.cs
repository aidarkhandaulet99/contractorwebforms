﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace ContractorWebForms.Pages
{
    public partial class LegalContrInfo : System.Web.UI.Page
    {
        string cs = ConfigurationManager.ConnectionStrings["dbCon"].ConnectionString;
        SqlConnection con;
        SqlCommand cmd;
        SqlDataAdapter adapter;
        DataTable dt;

        public void DataLoad()
        {
            if (Page.IsPostBack)
            {
                dgViewLegalCtr.DataBind();
            }
        }
        public void ClearAllData()
        {
            txtBIIN.Text = "";
            txtCrtDate.Text = DateTime.Today.Date.ToString();
            txtEdtDate.Text = DateTime.Today.Date.ToString();
            txtAuthor.Text = "";
            txtComName.Text = "";
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblSID.Text = dgViewLegalCtr.SelectedRow.Cells[1].Text;
            txtBIIN.Text = dgViewLegalCtr.SelectedRow.Cells[2].Text;
            txtCrtDate.Text = dgViewLegalCtr.SelectedRow.Cells[3].Text;
            txtEdtDate.Text = dgViewLegalCtr.SelectedRow.Cells[4].Text;
            txtAuthor.Text = dgViewLegalCtr.SelectedRow.Cells[5].Text;
            txtComName.Text = dgViewLegalCtr.SelectedRow.Cells[6].Text;
                      
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ClearAllData();
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            if (txtBIIN.Text!="" && txtAuthor.Text!="" && txtComName.Text!= "")
            {
                using(con = new SqlConnection(cs))
                {
                    con.Open();
                    cmd = new SqlCommand("INSERT INTO Legal_contractor(BIIN,Created_date,Edited_date,Created_by,Company_name) " +
                        "VALUES(@BIIN, @CreatedDate, @EditedDate, @CreatedBy, @CompanyName)", con);
                    cmd.Parameters.AddWithValue("@BIIN", txtBIIN.Text);
                    cmd.Parameters.AddWithValue("@CreatedDate", txtCrtDate.Text);
                    cmd.Parameters.AddWithValue("@EditedDate", txtEdtDate.Text);
                    cmd.Parameters.AddWithValue("@CreatedBy", txtAuthor.Text);
                    cmd.Parameters.AddWithValue("@CompanyName", txtComName.Text);
                    cmd.ExecuteNonQuery();
                    con.Close();
                    DataLoad();
                    ClearAllData();
                }
            }
            else
            {
                lblMessage.Text = "Fill all information";
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            if (txtBIIN.Text != "" && txtAuthor.Text != "" && txtComName.Text != "")
            {
                using (con = new SqlConnection(cs))
                {
                    con.Open();
                    cmd = new SqlCommand("UPDATE Legal_contractor SET BIIN=@BIIN,Created_date=@CreatedDate,Edited_date=@EditedDate," +
                        "Created_by=@EditedDate, Company_name=@CompanyName WHERE Id = @Id", con);
                        
                    cmd.Parameters.AddWithValue("@BIIN", txtBIIN.Text);
                    cmd.Parameters.AddWithValue("@CreatedDate", txtCrtDate.Text);
                    cmd.Parameters.AddWithValue("@EditedDate", txtEdtDate.Text);
                    cmd.Parameters.AddWithValue("@CreatedBy", txtAuthor.Text);
                    cmd.Parameters.AddWithValue("@CompanyName", txtComName.Text);
                    cmd.Parameters.AddWithValue("@Id", lblSID.Text);
                    cmd.ExecuteNonQuery();
                    con.Close();
                    DataLoad();
                    ClearAllData();
                }
            }
            else
            {
                lblMessage.Text = "Fill all information";
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            using (con = new SqlConnection(cs))
            {
                con.Open();
                cmd = new SqlCommand("DELETE FROM Legal_contractor WHERE Id=@Id", con);
                cmd.Parameters.AddWithValue("@Id", lblSID.Text);
                cmd.ExecuteNonQuery();
                con.Close();
                DataLoad();
                ClearAllData();

            }
        }
    }
}